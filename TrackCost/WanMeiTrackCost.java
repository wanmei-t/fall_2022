import java.util.Scanner;
public class WanMeiTrackCost
{
  public static void main(String[] args)
  {
    double radius1, radius2;
    Scanner scan = new Scanner(System.in);

    System.out.println("What is, in cm, the radius of the largest circle?");
    radius1 = scan.nextDouble();
    if (radius1 <= 0)
    {
      System.out.println("The value of your answer should be more than 0.");
      scan.close(); 
      return;
    }
    
    System.out.println("What is, in cm, the radius of the smallest circle?");
    radius2 = scan.nextDouble();
    if (radius2 <= 0)
    {
      System.out.println("The value of your answer should be more than 0.");
      scan.close(); 
      return;
    }
    
    if (radius1 <= radius2)
    {
      System.out.println("The value of your first answer can't be smaller or equal than the value of your second answer.");
      scan.close(); 
      return;
    }
    
    double radiuspow1, radiuspow2;
    radiuspow1 = Math.pow (radius1, 2);
    radiuspow2 = Math.pow (radius2, 2);
    double trackCost = (5 * (Math.PI * (radiuspow1 - radiuspow2)));
    System.out.print("The cost of the track will be $");
    System.out.printf("%.2f", trackCost);
    scan.close(); 
  }
}
